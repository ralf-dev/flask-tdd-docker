from flask import Blueprint
from flask_restx import Api, Resource

ping_blueprint = Blueprint("ping", __name__)
api = Api(ping_blueprint)


class Ping(Resource):
    @staticmethod
    def get() -> dict:
        return {"status": "success", "message": "pong!!!"}


api.add_resource(Ping, "/ping")
