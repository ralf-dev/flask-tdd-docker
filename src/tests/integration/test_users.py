import json

import pytest

from src.api.models import User


def test_add_user(test_app, test_db):
    client = test_app.test_client()
    resp = client.post(
        "/users",
        data=json.dumps({"username": "ralf", "email": "ralfvenchbillones@gmail.com"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())

    assert resp.status_code == 201
    assert "ralfvenchbillones@gmail.com" in data["message"]


def test_add_user_invalid_json(test_app, test_db):
    client = test_app.test_client()
    resp = client.post("/users", data=json.dumps({}), content_type="application/json")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 400
    assert "Input payload validation failed" in data["message"]


def test_add_user_invalid_json_keys(test_app, test_db):
    client = test_app.test_client()
    resp = client.post(
        "/users",
        data=json.dumps({"email": "ralf@gmail.com"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Input payload validation failed" in data["message"]


def test_add_user_duplicate_email(test_app, test_db):
    client = test_app.test_client()
    client.post(
        "/users",
        data=json.dumps(
            {"username": "michael", "email": "ralfvenchbillones@gmail.com"}
        ),
        content_type="application/json",
    )
    resp = client.post(
        "/users",
        data=json.dumps(
            {"username": "michael", "email": "ralfvenchbillones@gmail.com"}
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Sorry. That email already exists." in data["message"]


def test_single_user(test_app, test_db, add_user):
    user = add_user(username="ralf", email="ralfvenchbillones@gmail.com")
    client = test_app.test_client()
    resp = client.get(f"/users/{user.id}")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 200
    assert "ralf" in data["username"]
    assert "ralfvenchbillones@gmail" in data["email"]


def test_single_user_incorrect_id(test_app, test_db):
    client = test_app.test_client()
    resp = client.get(f"/users/{999}")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 404
    assert "User 999 does not exist" in data["message"]


def test_all_user(test_app, test_db, add_user):
    test_db.session.query(User).delete()
    add_user(username="ralf", email="ralfvenchbillones@gmail.com")
    add_user(username="ralf1", email="ralfvenchbillones1@gmail.com")

    client = test_app.test_client()
    resp = client.get("/users")
    data = json.loads(resp.data.decode())
    assert len(data) == 2


def test_remove_user(test_app, test_db, add_user):
    test_db.session.query(User).delete()
    user = add_user("user-to-be-remove", "remove-me@gmail.com")
    client = test_app.test_client()
    resp_one = client.get("/users")
    data = json.loads(resp_one.data.decode())
    assert resp_one.status_code == 200
    assert len(data) == 1

    resp_two = client.delete(f"/users/{user.id}")
    data = json.loads(resp_two.data.decode())
    assert resp_one.status_code == 200
    assert "remove-me@gmail.com was removed!" in data["message"]

    resp_three = client.get("/users")
    data = json.loads(resp_three.data.decode())
    assert resp_three.status_code == 200
    assert len(data) == 0


def test_remove_user_incorrect_id(test_app, test_db):
    client = test_app.test_client()
    resp = client.delete("/users/999")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "User 999 does not exist" in data["message"]


def test_update_user(test_app, test_db, add_user):
    user = add_user("user-to-be-updated", "update-me@gmail.com")
    client = test_app.test_client()
    resp_one = client.put(
        f"/users/{user.id}",
        data=json.dumps({"username": "me", "email": "me@gmail.com"}),
        content_type="application/json",
    )
    data = json.loads(resp_one.data.decode())
    assert resp_one.status_code == 200
    assert f"{user.id} was updated!" in data["message"]

    resp_two = client.get(f"/users/{user.id}")
    data = json.loads(resp_two.data.decode())
    assert resp_two.status_code == 200
    assert "me" in data["username"]
    assert "me@gmail.com" in data["email"]


@pytest.mark.parametrize(
    "user_id, payload, status_code, message",
    [
        [1, {}, 400, "Input payload validation failed"],
        [1, {"email": "me@testdriven.io"}, 400, "Input payload validation failed"],
        [
            999,
            {"username": "me", "email": "me@testdriven.io"},
            404,
            "User 999 does not exist",
        ],
    ],
)
def test_update_user_invalid(test_app, test_db, user_id, payload, status_code, message):
    client = test_app.test_client()
    resp = client.put(
        f"/users/{user_id}",
        data=json.dumps(payload),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == status_code
    assert message in data["message"]


def test_update_user_duplicate_email(test_app, test_db, add_user):
    add_user("hajek", "rob@hajek.org")
    user = add_user("rob", "rob@notreal.com")

    client = test_app.test_client()
    resp = client.put(
        f"/users/{user.id}",
        data=json.dumps({"username": "rob", "email": "rob@notreal.com"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Sorry. That email already exists." in data["message"]
